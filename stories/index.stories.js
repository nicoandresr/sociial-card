import React from 'react';

import src from './assets/sociial-card.png';
import SociialCard from '../';

export default {
  title: 'Example/SociialCard',
  component: SociialCard,
  argTypes: {
    src,
  },
};

const Template = (args) => <SociialCard {...args} />;

export const Primary = Template.bind({});
Primary.args = {src};

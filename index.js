import PropTypes from 'prop-types';
import React from 'react';

import 'twin.macro';

const propTypes = {
  alt: PropTypes.string,
  body: PropTypes.string,
  src: PropTypes.string.isRequired,
  title: PropTypes.string,
};

const defaultProps = {
  alt: 'sociial-card',
  title: '',
  body: '',
};

const SociialCard = (props) => (
  <figure
    tw="flex max-w-sm m-auto border-solid
      border flex-col rounded shadow-lg
      overflow-hidden"
  >
    <img src={props.src} alt={props.alt} tw="w-full" />
    <figcaption tw="px-6 py-4">
      <h1 tw="font-bold text-xl mb-2">{props.title}</h1>
      <p tw="text-gray-700 text-base">{props.body}</p>
    </figcaption>
  </figure>
);

SociialCard.propTypes = propTypes;
SociialCard.defaultProps = defaultProps;

export default SociialCard;
